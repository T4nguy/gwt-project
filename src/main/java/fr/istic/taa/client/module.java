package fr.istic.taa.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.core.client.EntryPoint;
import fr.istic.taa.client.widgets.associate.EpicAssociation;
import fr.istic.taa.client.widgets.associate.StoryAssociation;
import fr.istic.taa.client.widgets.associate.TaskAssociation;
import fr.istic.taa.client.widgets.create.*;

/**
 * Main Class. Define the interface with module.html.
 *
 */
public class module implements EntryPoint {

    /**
     * Initialize the IHM.
     */
    public void onModuleLoad() {

        MenuBar userMenu = new MenuBar(true);
        userMenu.addItem("Create", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final UserBinder userBinder = new UserBinder();
                RootPanel.get("content").add(userBinder);
            }
        });

        MenuBar epicMenu = new MenuBar(true);
        epicMenu.addItem("Create", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final EpicBinder epicBinder = new EpicBinder();
                RootPanel.get("content").add(epicBinder);
            }
        });
        epicMenu.addItem("Add Story", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final EpicAssociation epicAssoc = new EpicAssociation();
                RootPanel.get("content").add(epicAssoc);
            }
        });

        MenuBar storyMenu = new MenuBar(true);
        storyMenu.addItem("Create", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final StoryBinder storyBinder = new StoryBinder();
                RootPanel.get("content").add(storyBinder);
            }
        });
        storyMenu.addItem("Add Task", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final StoryAssociation storyAssoc = new StoryAssociation();
                RootPanel.get("content").add(storyAssoc);
            }
        });

        MenuBar taskMenu = new MenuBar(true);
        taskMenu.addItem("Create", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final TaskBinder taskBinder = new TaskBinder();
                RootPanel.get("content").add(taskBinder);
            }
        });
        taskMenu.addItem("Add User", new Command() {
            @Override
            public void execute() {
                RootPanel.get("content").clear();
                final TaskAssociation taskAssoc = new TaskAssociation();
                RootPanel.get("content").add(taskAssoc);
            }
        });

        // Create a menu bar to navigate in the interface.
        MenuBar menu = new MenuBar();


        menu.addItem("Epic", epicMenu);
        menu.addItem("Story", storyMenu);
        menu.addItem("Task", taskMenu);
        menu.addItem("User", userMenu);

        //Add menu bar in the RootPanel
        RootPanel.get("menu").add(menu);
    }

}
