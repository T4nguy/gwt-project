package fr.istic.taa.client.widgets.create;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractBinder for Task
 *
 */
public class TaskBinder extends AbstractBinder {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS = "http://myapp.taa.fr/task/create?name=";

    /**
     * Constructor method.
     * Personalize text in uiLabel.
     */
    public TaskBinder() {
        super();
        uiLabel.setText("Create Task:");
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS);
    }
}
