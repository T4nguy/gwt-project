package fr.istic.taa.client.widgets.create;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import fr.istic.taa.client.GreetingService;
import fr.istic.taa.client.GreetingServiceAsync;
import fr.istic.taa.shared.FieldVerifier;

/**
 * Abstract Class to regroup treatment.
 *
 */
public abstract class AbstractBinder extends Composite {

    private static AbstractBinderUiBinder uiBinder = GWT.create(AbstractBinderUiBinder.class);

    /**
     * To send Message.
     */
    protected final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

    /**
     * Text Field.
     */
    @UiField
    protected TextBox uiBox;

    /**
     * Text descriptor for uiBox.
     */
    @UiField
    protected Label uiLabel;

    /**
     * Information message.
     */
    @UiField
    protected Label informationLabel;

    /**
     * Button to send REST request.
     */
    @UiField
    protected Button uiButton;

    interface AbstractBinderUiBinder extends UiBinder<Widget, AbstractBinder> {
    }

    /**
     * Constructor method.
     */
    public AbstractBinder() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    /**
     * Connect the uiButton with his action.
     * @param event
     */
    @UiHandler("uiButton")
    public void doClickSubmit(ClickEvent event) {

    }





    /**
     * Send a REST Request at the address.
     * We create a new data with the content of the uiBox.
     * @param address address of the REST Server.
     */
    protected void sendTextToServer(String address){
        informationLabel.setText("");
        String textToServer = uiBox.getText();
        if (!FieldVerifier.isValidName(textToServer)) {
            informationLabel.getElement().getStyle().setColor("red");
            informationLabel.setText("Please, enter at least 4 characters.");
            return;
        }

        String val = uiBox.getValue();
        val = val.replace(' ', '+');
        String request = address + val;

        greetingService.greetServer(request, new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {

            }

            public void onSuccess(String result) {
                informationLabel.getElement().getStyle().setColor("green");
                informationLabel.setText(result);
            }
        });
        uiBox.setValue("");
    }

}