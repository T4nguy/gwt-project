package fr.istic.taa.client.widgets.create;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractBinder for Story.
 *
 */
public class StoryBinder extends AbstractBinder {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS = "http://myapp.taa.fr/story/create?name=";

    /**
     * Constructor method.
     * Personalize text in uiLabel.
     */
    public StoryBinder() {
        super();
        uiLabel.setText("Create Story:");
    }


    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS);
    }
}
