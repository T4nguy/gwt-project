package fr.istic.taa.client.widgets.create;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractBinder for User
 *
 */
public class UserBinder extends AbstractBinder {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS = "http://myapp.taa.fr/user/create?name=";

    /**
     * Constructor method.
     * Personalize text in uiLabel.
     */
    public UserBinder() {
        super();
        uiLabel.setText("Create User:");
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS);
    }
}