package fr.istic.taa.client.widgets.associate;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractAssociation for Story.
 *
 */
public class StoryAssociation extends AbstractAssociation {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS_TASK_ADD_STORY = "http://myapp.taa.fr/story/assign-task?idTask=";

    /**
     * Complement of the REST request
     */
    private final String COMPLETE_ADDRESS = "&idStory=";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_TASKS = "http://myapp.taa.fr/task/all";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_STORIES = "http://myapp.taa.fr/story/all";

    /**
     * Constructor method.
     * Personalize text in uiLabel and uiLabel2.
     * Allows to display lists.
     */
    public StoryAssociation() {
        super();
        uiLabel.setText("Tasks:");
        uiLabel2.setText("Stories:");
        fillIDLists(ADDRESS_DISPLAY_TASKS, ADDRESS_DISPLAY_STORIES);
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS_TASK_ADD_STORY, COMPLETE_ADDRESS);
    }

}
