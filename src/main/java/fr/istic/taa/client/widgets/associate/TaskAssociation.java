package fr.istic.taa.client.widgets.associate;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractAssociation for Task.
 *
 */
public class TaskAssociation extends AbstractAssociation {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS_TASK_ADD_USER = "http://myapp.taa.fr/task/assign-user?idUser=";

    /**
     * Complement of the REST request
     */
    private final String COMPLETE_ADDRESS = "&idTask=";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_TASKS = "http://myapp.taa.fr/task/all";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_USERS = "http://myapp.taa.fr/user/all";

    /**
     * Constructor method.
     * Personalize text in uiLabel and uiLabel2.
     * Allows to display lists.
     */
    public TaskAssociation() {
        super();
        uiLabel.setText("Users:");
        uiLabel2.setText("Tasks:");
        fillIDLists(ADDRESS_DISPLAY_USERS, ADDRESS_DISPLAY_TASKS);
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS_TASK_ADD_USER, COMPLETE_ADDRESS);
    }
}
