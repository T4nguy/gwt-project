package fr.istic.taa.client.widgets.associate;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dev.jjs.ast.js.JsonArray;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import fr.istic.taa.client.GreetingService;
import fr.istic.taa.client.GreetingServiceAsync;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Abstract Class to regroup treatment.
 *
 */
public abstract class AbstractAssociation extends Composite{

    private static AbstractAssociationUiBinder ourUiBinder = GWT.create(AbstractAssociationUiBinder.class);

    /**
     * To send Message.
     */
    protected final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

    /**
     * List to choose an element.
     */
    @UiField
    protected ListBox uiList;

    /**
     * List to choose an element.
     */
    @UiField
    protected Label res1;

    /**
     * Text descriptor for uiList.
     */
    @UiField
    protected Label uiLabel;

    /**
     * List to choose an other element.
     */
    @UiField
    protected ListBox uiList2;

    /**
     * Text descriptor for uiList2.
     */
    @UiField
    protected Label uiLabel2;

    /**
     * Error message (in red).
     */
    @UiField
    protected Label informationLabel;

    /**
     * Table to display all tasks with their users.
     */
    @UiField
    FlexTable uiTable;

    /**
     * Button to send REST request.
     */
    @UiField
    protected Button uiButton;

    /**
     * List of identifier
     */
    private List<Integer> idData;

    /**
     * List of identifier
     */
    private List<Integer> idData2;

    interface AbstractAssociationUiBinder extends UiBinder<Widget, AbstractAssociation> {
    }

    /**
     * Constructor method.
     */
    public AbstractAssociation() {
        initWidget(ourUiBinder.createAndBindUi(this));
        idData = new ArrayList<>();
        idData2 = new ArrayList<>();
    }

    /**
     * Connect the uiButton with his action.
     * @param event
     */
    @UiHandler("uiButton")
    public void doClickSubmit(ClickEvent event) {

    }

    /**
     * Compute the list for each idData and uiList.
     * @param add1
     * @param add2
     */
    protected void fillIDLists(String add1, String add2){
        returnList(add1, true);
        returnList(add2, false);
    }

    /**
     * Send a REST Request at the address.
     * We associate the content of the uiList with the uiList2.
     * @param address address of the REST Server.
     * @param complement complete address
     */
    protected void sendTextToServer(String address, String complement){
        informationLabel.setText("");
        int dataToServer = uiList.getSelectedIndex();
        int dataAssociate = uiList2.getSelectedIndex();
        if (dataToServer == -1 || dataAssociate == -1) {
            informationLabel.getElement().getStyle().setColor("red");
            informationLabel.setText("Please, select 2 values.");
            return;
        }

        String request = address + idData.get(dataToServer) + complement + idData2.get(dataAssociate);

        greetingService.greetServer(request, new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {

            }

            public void onSuccess(String result) {
                Window.alert("success: " + result);
                informationLabel.getElement().getStyle().setColor("green");
                informationLabel.setText(result);
            }
        });
    }

    /**
     * Send REST request to the server to display data list.
     *
     * @param address Address to access the good list
     * @param first First list
     */
    protected  void returnList(final String address, final boolean first) {

        greetingService.greetServer(address,new AsyncCallback<String>()
        {
            public void onFailure (Throwable caught){
                Window.alert("Error on the address: " + address);
            }

            public void onSuccess(String result) {



                JSONArray jsonObject = (JSONArray) JSONParser.parse(result);

                //Window.alert("toto: " + jsonObject);



                uiList.getElement().getStyle().setWidth(100, Style.Unit.PX);
                uiList2.getElement().getStyle().setWidth(100, Style.Unit.PX);

                uiTable.setText(0, 0, "Element");
                uiTable.setText(0, 1, "Content");
                uiTable.getColumnFormatter().setWidth(0, "100px");
                uiTable.getColumnFormatter().setWidth(1, "100px");
                int rowCount = 1;

                for(int i = 0; i < jsonObject.size(); i++){

                    JSONValue str = jsonObject.get(i);
                    JSONValue yo = str.isObject().get("name");
                    JSONValue yoInt = str.isObject().get("id");
                    if(first) {
                        uiList.addItem(yo.toString());
                        idData.add(Integer.parseInt(yoInt.toString()));
                    }
                    else{
                        uiList2.addItem(yo.toString());
                        idData2.add(Integer.parseInt(yoInt.toString()));
                    }


                }

                String display="";

                for(int i = 0; i < jsonObject.size(); i++){

                    JSONValue str = jsonObject.get(i);

                    if(!first) {
                        JSONValue epicName = str.isObject().get("name");

                        JSONArray myArray = null;
                        if(str.isObject().get("stories")!=null){
                            myArray =str.isObject().get("stories").isArray();
                        }
                        else if(str.isObject().get("tasks")!=null){
                            myArray =str.isObject().get("tasks").isArray();
                        }
                        else if(str.isObject().get("users")!=null){
                            myArray =str.isObject().get("users").isArray();
                        }



                        String elemetns="";



                        for(int j = 0; j < myArray.size(); j++) {

                            JSONValue str2 = myArray.get(j);

                            JSONValue storyName = str2.isObject().get("name");
                            elemetns+=storyName+", ";
                        }

                        uiTable.setText(rowCount, 1, elemetns);
                        uiTable.setText(rowCount, 0, epicName.toString());
                        rowCount+=1;



                        //display+="\n" +epicName.toString()+ "=>" +epicStories.toString() ;

                    }



                }


                if(!first) {
                    //informationLabel.setText(display);
                }


                /**if(!first) {
                    res1.getElement().getStyle().setColor("purple");
                    informationLabel.setText(jsonObject.toString());
                }**/
            }
        });
    }
}
