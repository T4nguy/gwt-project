package fr.istic.taa.client.widgets.create;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractBinder for Epic
 *
 */
public class EpicBinder extends AbstractBinder {

    /**
     * Address to execute REST request
     */
    private final String ADDRESS = "http://myapp.taa.fr/epic/create?name=";

    /**
     * Constructor method.
     * Personalize text in uiLabel.
     */
    public EpicBinder() {
        super();
        uiLabel.setText("Create Epic:");
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS);
    }
}
