package fr.istic.taa.client.widgets.associate;

import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Class to complete AbstractAssociation for Epic.
 *
 */
public class EpicAssociation extends AbstractAssociation{

    /**
     * Address to execute REST request
     */
    private final String ADDRESS_EPIC_ADD_STORY = "http://myapp.taa.fr/epic/assign-story?idStory=";

    /**
     * Complement of the REST request
     */
    private final String COMPLETE_ADDRESS = "&idEpic=";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_STORIES = "http://myapp.taa.fr/story/all";

    /**
     * REST request to access data
     */
    private final String ADDRESS_DISPLAY_EPICS = "http://myapp.taa.fr/epic/all";


    /**
     * Constructor method.
     * Personalize text in uiLabel and uiLabel2.
     * Allows to display lists.
     */
    public EpicAssociation() {
        super();
        uiLabel.setText("Stories:");
        uiLabel2.setText("Epics:");

        fillIDLists(ADDRESS_DISPLAY_STORIES, ADDRESS_DISPLAY_EPICS);
    }

    /**
     * Action when users click on the uiButton.
     * @param event
     */
    @Override
    public void doClickSubmit(ClickEvent event) {
        sendTextToServer(ADDRESS_EPIC_ADD_STORY, COMPLETE_ADDRESS);
    }
}
