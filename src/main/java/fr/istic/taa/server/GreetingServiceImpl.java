package fr.istic.taa.server;

import fr.istic.taa.client.GreetingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
    GreetingService {

  /**
   * Try to establish a connection with the input address.
   * @param input Address
   * @return The return string of the server.
   */
  public String greetServer(String input) {

    StringBuffer str = new StringBuffer();
    try {
      URL obj = new URL(input);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      if(con.getResponseCode() != 503) {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
          str.append(inputLine);
        }
        in.close();
      } else {
        return "";
      }
    } catch (IOException e) {
      return "";
    }

    return str.toString();
  }
}
