# GLI - GWT #

**BERNARD Tanguy**

### Objectif ###

L'objectif de ce tp est de développer un client en **GWT** qui exécutera des requêtes vers le service REST mis en place en [TAA](https://bitbucket.org/T4nguy/agile-app).

### Installation ###

1. Exécuter le service REST (voir projet TAA)
2. Récupérer le projet
3. ```mvn gwt:run```

### Description du projet ###

Application ayant pour but de gérer un projet par les méthodes agiles.
L'utilisateur peut créer des epics, stories, et tasks, et par la suite assigner des stories à une epic. Il pourra attribuer un utilisateur à une tâche.


![assigned_story.png](https://bitbucket.org/repo/Xy65rr/images/2159058966-assigned_story.png)



